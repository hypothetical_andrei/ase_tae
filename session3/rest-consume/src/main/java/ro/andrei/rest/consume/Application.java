package ro.andrei.rest.consume;

import org.springframework.web.client.RestTemplate;
import ro.andrei.rest.consume.entities.Book;
import ro.andrei.rest.consume.entities.Category;

public class Application {
    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        Category category = restTemplate.getForObject("http://localhost:8080/categories/1", Category.class);
        System.out.println("cat : " + category.getName());
        for (Book b : category.getBooks()) {
            System.out.println("book : " + b.getTitle());
        }
    }
    
}
