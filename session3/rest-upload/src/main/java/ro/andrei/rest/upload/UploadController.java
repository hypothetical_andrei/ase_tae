package ro.andrei.rest.upload;

import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ro.andrei.rest.upload.storage.StorageException;
import ro.andrei.rest.upload.storage.StorageService;

@Controller
public class UploadController {

    private final StorageService storageService;

    @Autowired
    public UploadController(StorageService storageService) {
        this.storageService = storageService;
        this.storageService.init();
    }
    
    @GetMapping("/")
    public String listUploadedFiles(Model model) {
        model.addAttribute("files", storageService
                .loadAll()
                .map(p -> "files/" + p)
                .collect(Collectors.toList()));
        return "upload";
    }
        
    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file) {
        storageService.store(file);
        return "upload";
    }
    
    @ExceptionHandler(StorageException.class)
    public ResponseEntity handleStorageException(StorageException exc) {
        return ResponseEntity.badRequest().build();
    }

}
