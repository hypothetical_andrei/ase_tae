package ro.andrei.books.rest.jpa;

import java.util.Iterator;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.andrei.books.rest.jpa.entities.Book;
import ro.andrei.books.rest.jpa.entities.Category;
import ro.andrei.books.rest.jpa.entities.CategoryRepository;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryRepository repo;

    @Autowired
    public CategoryController(CategoryRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Category>> getCategories() {
        return ResponseEntity.ok(repo.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> getCategory(@PathVariable(value = "id") long id) {
        Category k = repo.findOne(id);
        return ResponseEntity.ok(k);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addCategory(@RequestBody Category category) {
        repo.save(new Category(category.getName()));
        return ResponseEntity.accepted().build();
    }

    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCategory(@PathVariable(value = "id") long id) {
        repo.delete(id);
        return ResponseEntity.accepted().build();
    }

    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceCategory(@PathVariable(value = "id") long id, @RequestBody Category category) {
        Category c = repo.findOne(id);
        if (c != null) {
            return ResponseEntity.accepted().build();
        } else {
            c.setName(category.getName());
            repo.save(c);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/{id}/books", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Book>> getBooks(@PathVariable(value = "id") long id) {
        Category c = repo.findOne(id);
        List<Book> books = c.getBooks();
        return ResponseEntity.ok(books);
    }

    @RequestMapping(value = "/{id}/books/{book_id}", method = RequestMethod.GET)
    public ResponseEntity<Book> getBook(@PathVariable(value = "id") long id, @PathVariable(value = "book_id") long bookId) {
        Category c = repo.findOne(id);
        Book book = c.getBooks().stream().filter(b -> b.getId() == bookId).findFirst().get();
        return ResponseEntity.ok(book);
    }

    @Transactional
    @RequestMapping(value = "/{id}/books", method = RequestMethod.POST)
    public ResponseEntity<?> addBook(@PathVariable(value = "id") long id, @RequestBody Book book) {
        Category c = repo.findOne(id);
        Book newBook = new Book(book.getTitle(), book.getAuthor());
        newBook.setCategory(c);
        List<Book> books = c.getBooks();
        books.add(newBook);
        c.setBooks(books);
        repo.save(c);
        return ResponseEntity.accepted().build();
    }

    @Transactional
    @RequestMapping(value = "/{id}/books/{book_id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") long id, @PathVariable(value = "book_id") long bookId) {
        Category c = repo.findOne(id);
        List<Book> books = c.getBooks();
        books.removeIf(b -> b.getId() == bookId);
//        for (Iterator<Book> iterator = books.iterator(); iterator.hasNext();) {
//            Book next = iterator.next();
//            if(next.getId() == bookId){
//                System.err.println("found " + bookId + ". removing");
//                iterator.remove();
//            }
//        }
        c.setBooks(books);
        repo.save(c);
        return ResponseEntity.accepted().build();
    }

    @Transactional
    @RequestMapping(value = "/{id}/books/{book_id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceBook(@PathVariable(value = "id") long id, @PathVariable(value = "book_id") long bookId, @RequestBody Book book) {
        Book oldBook = repo.findOne(id).getBooks().stream().filter(b -> b.getId() == bookId).findFirst().get();
        oldBook.setTitle(book.getTitle());
        oldBook.setAuthor(book.getAuthor());
        return ResponseEntity.notFound().build();
    }
}
