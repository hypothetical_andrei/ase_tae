package ro.andrei.spring.thyme.crud.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.andrei.spring.thyme.crud.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
    
}
