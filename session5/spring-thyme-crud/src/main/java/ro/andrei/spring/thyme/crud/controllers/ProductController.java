package ro.andrei.spring.thyme.crud.controllers;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ro.andrei.spring.thyme.crud.entities.Product;
import ro.andrei.spring.thyme.crud.entities.dao.ProductRepository;

@Controller
public class ProductController {

    private ProductRepository repo;

    @Autowired
    public ProductController(ProductRepository repo) {
        this.repo = repo;
    }
    
    @GetMapping("/")
    public String getProducts(Model model) {
        model.addAttribute("products", repo.findAll());
        model.addAttribute("product", new Product());
        return "products";
    }
    
    @PostMapping("/")
    @Transactional
    public String addProduct(@ModelAttribute Product product) {
        repo.save(product);
        return "redirect:/";
    }
    
    @GetMapping("/{id}")
    public String selectProduct(@PathVariable("id") Long id, HttpSession session) {
        session.setAttribute("current", repo.findOne(id));
        return "redirect:/";
    }
    
    @DeleteMapping("/{id}")
    @Transactional
    public String deleteProduct(@PathVariable("id") Long id) {
        repo.delete(id);
        return "redirect:/";
    }
    
    @PostMapping("/{id}/save")
    @Transactional
    public String saveProduct(@PathVariable("id") Long id, @ModelAttribute Product product, HttpSession session) {
        System.err.println("why do you call?");
        Product p = repo.findOne(id);
        p.setName(product.getName());
        p.setPrice(product.getPrice());
        p.setDescription(product.getDescription());
        p.setImage(product.getImage());
        session.invalidate();
        return "redirect:/";
    }

}
