package ro.andrei.spring.thyme.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ro.andrei.spring.thyme.entities.Bundle;
import ro.andrei.spring.thyme.entities.Product;
import ro.andrei.spring.thyme.entities.dao.OrderRepository;
import ro.andrei.spring.thyme.entities.dao.ProductRepository;

@Controller
public class ShopController {

    private final ProductRepository productRepo;
    private final OrderRepository orderRepo; 
    
    @Autowired
    public ShopController(ProductRepository productRepo, OrderRepository orderRepo) {
        this.productRepo = productRepo;
        this.orderRepo = orderRepo;
    }
    
    @GetMapping("/")
    public String getProducts(Model model){
        List<Product> products = productRepo.findAll();
//        List<Product> products = new ArrayList<>();
//        products.add(new Product(1L, "test", 10.0, "some product", "some image"));
        model.addAttribute("products", products);
        return "index";
    }
    
    @GetMapping("/add")
    public String addToCart(@RequestParam Long id, HttpSession session){
        Product p = productRepo.findOne(id);
        List<Product> products = (List<Product>) session.getAttribute("cart");
        if (products == null){
            products = new ArrayList<>();
        }
        products.add(p);
        session.setAttribute("cart", products);
        return "redirect:/";
    }
    
    @GetMapping("/buy")
    public String buy(HttpSession session){
        List<Product> products = (List<Product>) session.getAttribute("cart");
        Bundle o = new Bundle();
        o.setName("some order");
        o.setProducts(products);
        orderRepo.save(o);
        session.invalidate();
        return "redirect:/";    
    }
    
}
