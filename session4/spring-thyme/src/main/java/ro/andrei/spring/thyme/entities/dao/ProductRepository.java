package ro.andrei.spring.thyme.entities.dao;

import ro.andrei.spring.thyme.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long>{
    
}
