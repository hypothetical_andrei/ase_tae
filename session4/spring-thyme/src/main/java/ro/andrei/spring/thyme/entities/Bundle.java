package ro.andrei.spring.thyme.entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor @NoArgsConstructor
public class Bundle {

    @Id @GeneratedValue(strategy = GenerationType.AUTO) 
    @Getter @Setter private Long id;  
    
    @Getter @Setter private String name;  
    
    @Getter @Setter
    @ManyToMany
    private List<Product> products;
    
}
