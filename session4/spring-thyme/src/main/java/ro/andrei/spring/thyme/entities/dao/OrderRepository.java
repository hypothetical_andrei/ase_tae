package ro.andrei.spring.thyme.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.andrei.spring.thyme.entities.Bundle;

public interface OrderRepository extends JpaRepository<Bundle, Long>{
    
}
