package ro.andrei.spring.thyme.entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor @NoArgsConstructor
public class Product {


    @Id @GeneratedValue(strategy = GenerationType.AUTO) 
    @Getter @Setter private Long id;    
    
    @Getter @Setter private String name;
    @Getter @Setter private Double price;
    @Getter @Setter private String description;
    @Getter @Setter private String image;    
    
    @ManyToMany(mappedBy = "products")
    private List<Bundle> orders;
}
