package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListDepartmentsServlet", urlPatterns = {"/departments"})
public class ListDepartmentsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection connection = null;
        ResultSet results = null;
        try {
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/servlet_employees", "root", "root");
            Statement statement = connection.createStatement();
            results = statement.executeQuery("select * from departments");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>List of departments</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Departments</h1>");
            out.println("<table>");
            while (results.next()) {
                out.println("<tr>");
                out.println("<td><a href='details?dep=" + results.getString("department_name")
                        + "'>" + results.getString("department_name") + "</a></td>");
                out.println("<td>" + results.getString("department_description") + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        } catch (SQLException ex) {
            Logger.getLogger(ListDepartmentsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
