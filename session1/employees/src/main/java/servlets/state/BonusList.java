package servlets.state;

import java.util.ArrayList;
import java.util.List;

public class BonusList {

    private List<EmployeeBonus> bonusList;

    public BonusList() {
        bonusList = new ArrayList<EmployeeBonus>();
    }

    public void addBonus(String name, String department) {
        bonusList.add(new EmployeeBonus(name, department));
    }

    public List<EmployeeBonus> getBonuses() {
        return bonusList;
    }
}
