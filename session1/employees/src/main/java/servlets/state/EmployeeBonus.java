package servlets.state;

public class EmployeeBonus {
    private String name;
    private String department;
    
    public EmployeeBonus(String name, String deparment){
        this.name = name;
        this.department = deparment;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }
}
