package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DepartmentDetailsServlet", urlPatterns = {"/details"})
public class DepartmentDetailsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String department = request.getParameter("dep");
        Connection connection = null;
        ResultSet results = null;
        try {
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/servlet_employees", "root", "root");
            Statement statement = connection.createStatement();
            results = statement.executeQuery("select * from employees e inner join jobs j on e.job_id = j.job_id inner join departments d on j.department_id = d.department_id and department_name = '" + department + "'");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>List of employees</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Employees in department " + department + "</h1>");
            out.println("<table>");
            while (results.next()) {
                out.println("<tr>");
                out.println("<td>" + results.getString("employee_name") + "</td>");
                out.println("<td>" + results.getString("job_title") + "</td>");
                out.println("<td><a href='bonus?employee_name=" + results.getString("employee_name")
                        + "&employee_department=" + department + "'>Add to bonus</a></td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
