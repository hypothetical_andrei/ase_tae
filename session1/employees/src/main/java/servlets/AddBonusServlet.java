package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servlets.state.BonusList;
import servlets.state.EmployeeBonus;

@WebServlet(name = "AddBonusServlet", urlPatterns = {"/bonus"})
public class AddBonusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        BonusList bonuses = (BonusList) session.getAttribute("bonuses");
        if (bonuses == null) {
            bonuses = new BonusList();
            bonuses.addBonus(request.getParameter("employee_name"), request.getParameter("employee_department"));
        } else {
            bonuses.addBonus(request.getParameter("employee_name"), request.getParameter("employee_department"));
        }
        session.setAttribute("bonuses", bonuses);
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddBonusServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Employees that will get a bonus</h1>");
            out.println("<table>");
            for (EmployeeBonus employee : bonuses.getBonuses()) {
                out.println("<tr>");
                out.println("<td>" + employee.getName() + "</td>");
                out.println("<td>" + employee.getDepartment() + "</td>");
                out.println("</tr>");
            }
            out.println("</table>");
            out.println("<a href='departments'>Back to departments</a>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
