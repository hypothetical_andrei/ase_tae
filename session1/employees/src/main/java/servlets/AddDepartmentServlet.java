package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AddDepartmentServlet", urlPatterns = {"/add"})
public class AddDepartmentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection connection = null;
        ResultSet results = null;
        try {
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/servlet_employees", "root", "root");
            PreparedStatement statement = connection.prepareStatement("insert into departments "
                    + "(department_id, department_name, department_description) values (?,?,?)");
            statement.setInt(1, Integer.parseInt(request.getParameter("department_id")));
            statement.setString(2, request.getParameter("department_name"));
            statement.setString(3, request.getParameter("department_description"));
            statement.executeUpdate();
            response.sendRedirect("departments");
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
