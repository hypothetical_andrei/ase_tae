import React, { Component } from 'react'

class Kitten extends Component {
	constructor(props){
		super(props)
		this.state = {
			isEditing : false,
			kittenName : this.props.kittenName
		}
		this.onChange = this.onChange.bind(this)
	}
	
	onChange(e){
		this.setState({
			kittenName : e.target.value
		})
	}

	render() {
		if (!this.state.isEditing){
			return (
				<li className="KittenList">
					<div>
						Hi, i am {this.props.kittenName}, a kitten!
						<form>
							<input type="button" value="del" onClick={this.props.onDelete} />
							<input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
						</form>
					</div>
				</li>
			)
		}
		else{
			return (
				<li>
					<div>
						I can change <input type="text" value={this.state.kittenName} onChange={this.onChange} />!
						<form>
							<input type="button" value="save" onClick={() => {this.props.onSave(this.state.kittenName, this.props.kittenId);this.setState({isEditing : false})}} />
							<input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
						</form>
					</div>
				</li>
			)
		}
	}
}

export default Kitten
