import React, { Component } from 'react'
import Kitten from './Kitten'
import KittenStore from '../stores/KittenStore'
import {EventEmitter} from 'fbemitter'

const emitter = new EventEmitter()
const store = new KittenStore(emitter)

const deleteKitten = (id) => {
	store.deleteOne(id)
}

const addKitten = (kitten) => {
	store.addOne({name : kitten})
}

const saveKitten = (kitten, id) => {
	store.saveOne({name : kitten}, id)
}

class KittenList extends Component {
	constructor(props) {
		super(props)
		this.state = {
				kittens: [],
				kittenName : ''
			}
		this.onChange = this.onChange.bind(this)
	}

	componentDidMount() {
		store.getAll()
		emitter.addListener('KITTEN_LOAD', () => {
			this.setState({kittens : store.content})
		})
	}

	onChange(e){
		this.setState({
			kittenName : e.target.value
		})
	}

	render() {
		return (
			<div className="KittenList">
				<div>
					<h3>Kittens</h3>
						<ul>
							{
								this.state.kittens.map(k => 
									<Kitten 
										key={k.id}
										kittenId={k.id}
										kittenName={k.name}
										onDelete={() => deleteKitten(k.id)}
										onSave={saveKitten}
									/>
								)
							}
					</ul>
					<form>
						Add a new kitten: <input type="text" value={this.state.kittenName} onChange={this.onChange}/>
						<input type="button" value="add" onClick={() => addKitten(this.state.kittenName)} />
					</form>
				</div>
  		</div>
		)
	}
	
}


export default KittenList
