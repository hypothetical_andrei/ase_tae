import React, { Component } from 'react'
// import './App.css'
import KittenList from './components/KittenList'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Kittens!!!</h2>
          <KittenList />
        </div>
      </div>
    )
  }
}

export default App
