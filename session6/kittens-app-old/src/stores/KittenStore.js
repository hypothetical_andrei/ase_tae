import axios from 'axios'

const SERVER = 'http://localhost:8080'


class KittenStore{
	constructor(ee){
		// this.content = [{id : 1, name : 'timmy'}]
		this.emitter = ee
		this.content = []
	}
	getAll(){
		axios(SERVER + '/kitten')
			.then((response) => {
				this.content = response.data
				this.emitter.emit('KITTEN_LOAD')
			})
			.catch((error) => {
				console.warn(error)
			})
	}
	deleteOne(id){
		axios.delete(SERVER + '/kitten/' + id)
			.then(() => {
				this.getAll()
			})
			.catch((error) => {
				console.warn(error)
			})	
	}
	addOne(kitten){
		axios({
				method:'post',
				url : SERVER + '/kitten',
			  headers: {'Content-Type': 'application/json'},
				data : kitten
			})
			.then(() => {
				this.getAll()
			})
			.catch((error) => {
				console.warn(error)
			})	
	}
	saveOne(kitten, id){
		axios({
				method:'put',
				url : SERVER + '/kitten/' + id,
			  headers: {'Content-Type': 'application/json'},
				data : kitten
			})
			.then(() => {
				this.getAll()
			})
			.catch((error) => {
				console.warn(error)
			})	
	}
}

export default KittenStore