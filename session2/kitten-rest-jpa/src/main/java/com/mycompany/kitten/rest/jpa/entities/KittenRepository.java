/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kitten.rest.jpa.entities;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author null
 */
public interface KittenRepository extends CrudRepository<Kitten, Long>{
    List<Kitten> findByName(String name);
}
