package com.mycompany.kitten.rest.jpa;

import com.mycompany.kitten.rest.jpa.entities.Kitten;
import com.mycompany.kitten.rest.jpa.entities.KittenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kitten")
public class KittenController {

    private final KittenRepository repo;

    @Autowired
    public KittenController(KittenRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Kitten>> getKittens() {
        return ResponseEntity.ok(repo.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Kitten> getKitten(@PathVariable(value = "id") long id) {
        Kitten k = repo.findOne(id);
        return ResponseEntity.ok(k);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addKitten(@RequestBody Kitten kitten) {
        repo.save(new Kitten(0L, kitten.getName()));
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteKitten(@PathVariable(value = "id") long id) {
        repo.delete(id);
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceKitten(@PathVariable(value = "id") long id, @RequestBody Kitten kitten) {
        Kitten k = repo.findOne(id);
        k.setName(kitten.getName());
        repo.save(k);
        if (k != null) {
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
