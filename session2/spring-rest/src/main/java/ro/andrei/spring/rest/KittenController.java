package ro.andrei.spring.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ro.andrei.spring.rest.entities.Kitten;

@RestController
@RequestMapping("/kitten")
public class KittenController {

    private final AtomicLong counter = new AtomicLong();
    private List<Kitten> kittens;

    public KittenController() {
        this.kittens = new ArrayList();
        kittens.add(new Kitten(counter.incrementAndGet(), "timmy"));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Kitten>> getKittens() {
        return ResponseEntity.ok(kittens);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Kitten> getKitten(@PathVariable(value = "name") long id) {
        return ResponseEntity.ok(kittens.stream().filter(k -> k.getId() == id).findFirst().get());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addKitten(@RequestBody Kitten kitten) {
        long id = counter.incrementAndGet();
        kittens.add(new Kitten(id, kitten.getName()));
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(kitten.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteKitten(@PathVariable(value = "id") long id) {
        kittens.remove(kittens.stream().filter(k -> k.getId() == id).findFirst().get());
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceKitten(@PathVariable(value = "id") long id, @RequestBody Kitten kitten) {
        Kitten oldKitten = kittens.stream().filter(k -> k.getId() == id).findFirst().get();
        long oldId = oldKitten.getId();
        kittens.remove(oldKitten);
        kittens.add(new Kitten(oldId, kitten.getName()));
        return ResponseEntity.accepted().build();
    }
//    @RequestMapping(path = "/kitten", method = RequestMethod.POST)
//    public Kitten greeting(@RequestParam(value = "name", defaultValue = "timmy") String name) {
//        return kittens.stream().filter(k -> (k.getName() == null ? name == null : k.getName().equals(name))).findFirst().get();
//    }
}
