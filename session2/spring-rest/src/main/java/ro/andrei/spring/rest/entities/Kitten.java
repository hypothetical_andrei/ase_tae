package ro.andrei.spring.rest.entities;


public class Kitten {

    private final long id;
    private final String name;

    public Kitten() {
        this.id = 0;
        this.name = null;
    }
    
    public Kitten(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
